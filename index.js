const express = require("express");
const app = express();
const port = 8000;

const drinkRouter = require("./app/routes/drinkRouter");
const vourcherRouter = require("./app/routes/vourcherRouter");
const userRouter = require("./app/routes/userRouter");
const orderRouter = require("./app/routes/orderRouter");

app.use(drinkRouter);
app.use(vourcherRouter);
app.use(userRouter);
app.use(orderRouter);

app.listen(port, () => {
    console.log("App listening on port: ", port);
});