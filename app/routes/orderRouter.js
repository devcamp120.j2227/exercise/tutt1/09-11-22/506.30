const express = require("express");
const router = express.Router();

router.get("/orders", (req, res) => {
    res.status(200).json({
        message: `Get all orders`
    });
});

router.get("/orders/:orderId", (req, res) => {
    let orderId = req.params.orderId;
    res.status(200).json({
        message: `Get orderId = ${orderId}`
    });
});

router.post("/orders", (req, res) => {
    res.status(200).json({
        message: `Create user`
    });
});

router.put("/orders/:orderId", (req, res) => {
    let orderId = req.params.orderId;
    res.status(200).json({
        message: `Update orderId = ${orderId}`
    });
});

router.delete("/orders/:orderId", (req, res) => {
    let orderId = req.params.orderId;
    res.status(200).json({
        message: `Delete orderId = ${orderId}`
    });
});

module.exports = router;