const express = require("express");
const router = express.Router();

router.get("/drinks", (req, res) => {
    res.status(200).json({
        message: `Get all drinks`
    });
});

router.get("/drinks/:drinkId", (req, res) => {
    let drinkId = req.params.drinkId;
    res.status(200).json({
        message: `Get drinkId = ${drinkId}`
    });
});

router.post("/drinks", (req, res) => {
    res.status(200).json({
        message: `Create drink`
    });
});

router.put("/drinks/:drinkId", (req, res) => {
    let drinkId = req.params.drinkId;
    res.status(200).json({
        message: `Update drinkId = ${drinkId}`
    });
});

router.delete("/drinks/:drinkId", (req, res) => {
    let drinkId = req.params.drinkId;
    res.status(200).json({
        message: `Delete drinkId = ${drinkId}`
    });
});

module.exports = router;