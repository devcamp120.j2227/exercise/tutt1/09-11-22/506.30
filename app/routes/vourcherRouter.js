const express = require("express");
const router = express.Router();

router.get("/vourcher", (req, res) => {
    res.status(200).json({
        message: `Get all vourcher`
    });
});

router.get("/vourcher/:vourcherId", (req, res) => {
    let vourcherId = req.params.vourcherId;
    res.status(200).json({
        message: `Get vourcherId = ${vourcherId}`
    });
});

router.post("/vourcher", (req, res) => {
    res.status(200).json({
        message: `Create vourcher`
    });
});

router.put("/vourcher/:vourcherId", (req, res) => {
    let vourcherId = req.params.vourcherId;
    res.status(200).json({
        message: `Update vourcherId = ${vourcherId}`
    });
});

router.delete("/vourcher/:vourcherId", (req, res) => {
    let vourcherId = req.params.vourcherId;
    res.status(200).json({
        message: `Delete vourcherId = ${vourcherId}`
    });
});

module.exports = router;